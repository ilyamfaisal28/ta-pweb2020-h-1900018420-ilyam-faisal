function klik_logout(){
			var tes = confirm("Apakah Anda yakin ingin Log Out?")
				if (tes === true) {
					alert("Selamat Anda berhasil Log Out!")
					window.location="login.php"
				}
				else{
					window.location="beranda.php"
				}
			}


if (document.readyState == 'loading') {
	document.addEventListener('DOMContentLoaded', ready)
}
else {
	ready()
}

function ready(){
	var TombolHapus = document.getElementsByClassName('btn-hapus')
	console.log(TombolHapus)
	for (var i = 0; i < TombolHapus.length; i++) {
		var tombol = TombolHapus[i]
		tombol.addEventListener('click', hapusItem)
	}
	var kuantitasInput = document.getElementsByClassName('cart-kuantitas-input')
	for(var i = 0; i < kuantitasInput.length; i++){
		var input = kuantitasInput[i]
		input.addEventListener('change', kuantitasBerubah)
	}
	var tombolTambahKeranjang = document.getElementsByClassName('tombolkrj')
	for(var i= 0; i < tombolTambahKeranjang.length; i++){
		var tombol = tombolTambahKeranjang[i]
		tombol.addEventListener('click', tombolTambahKeranjangTerklik)
	}
	document.getElementsByClassName('btn-beli')[0].addEventListener('click', BeliTerklik)
}

function BeliTerklik(){
	alert('Terima kasih telah membeli di ILY.am :)')	
	var cartBarang = document.getElementsByClassName('cart-all-item')[0]
	while (cartBarang.hasChildNodes()) {
		cartBarang.removeChild(cartBarang.firstChild)
	}
	updateTotal()
}

function hapusItem(event){
	var tombol_terklik = event.target
	tombol_terklik.parentElement.parentElement.remove()
	updateTotal()
}

function kuantitasBerubah(event){
	var input = event.target
	if (isNaN(input.value) || input.value <= 0) {
		input.value = 1
	}
	updateTotal()
}

function tombolTambahKeranjangTerklik(){
	var tombol = event.target
	var barang = tombol.parentElement.parentElement
	var judulbrg = barang.getElementsByClassName('jdperbarang')[0].innerText
	var harga = barang.getElementsByClassName('harga')[0].innerText
	var gambarbrg = barang.getElementsByClassName('gambar-barang')[0].src
	tambahBarangKeKeranjang(judulbrg, harga, gambarbrg)
	updateTotal()
}

function tambahBarangKeKeranjang(judulbrg, harga, gambarbrg){
	var cartBaris = document.createElement('div')
	cartBaris.classList.add('cart-baris')
	var cartBarang = document.getElementsByClassName('cart-all-item')[0]
	var cartBarangNama = cartBarang.getElementsByClassName('cart-judul-item')
	for(var i = 0; i < cartBarangNama.length; i++){
		if (cartBarangNama[i].innerText == judulbrg) {
			alert('Barang ini sudah ditambahkan ke Keranjang. Silahkan lihat Keranjang di paling bawah web ini!')
			return
		}
	}
	var cartBarisContent = `
		<div class="cart-item cart-kolom">
			<img class="cart-gbr" src="${gambarbrg}" height="100">
			<span class="cart-judul-item">${judulbrg}</span>
		</div>
		<span class="cart-harga cart-kolom">${harga}</span>
		<div class="class-kuantitas cart-kolom">
			<input class="cart-kuantitas-input" type="number" value="1">
        	<button class="btn btn-hapus" type="button">Hapus</button>
		</div>`
	cartBaris.innerHTML = cartBarisContent 
	cartBarang.append(cartBaris)
	cartBaris.getElementsByClassName('btn-hapus')[0].addEventListener('click', hapusItem)
	cartBaris.getElementsByClassName('cart-kuantitas-input')[0].addEventListener('change', kuantitasBerubah)
}

function updateTotal(){
	var allItemContainer = document.getElementsByClassName('cart-all-item')[0]
	var cartSemuaBaris = allItemContainer.getElementsByClassName('cart-baris')
	var totalHarga = 0 
	for (var i = 0; i < cartSemuaBaris.length; i++) {
		var cartBaris = cartSemuaBaris[i]
		var hargaElemen = cartBaris.getElementsByClassName('cart-harga')[0]
		var kuantitasElemen = cartBaris.getElementsByClassName('cart-kuantitas-input')[0]
		var harga = parseFloat(hargaElemen.innerText.replace('Rp.', ''))
		var kuantitas = kuantitasElemen.value
		totalHarga = totalHarga + (harga*kuantitas)
	}
	document.getElementsByClassName('cart-total-harga')[0].innerText = 'Rp.'+ totalHarga + 'rb'
}

