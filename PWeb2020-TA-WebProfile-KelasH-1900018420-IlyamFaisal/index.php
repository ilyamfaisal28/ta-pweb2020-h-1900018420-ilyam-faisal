<!DOCTYPE html>
<html>
<head>
	<title>Beranda</title>
	<link rel="stylesheet" type="text/css" href="style3.css">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

</head>
<body>
	<script type="text/javascript">
		function klik_logout(){
			var tes = confirm("Apakah Anda yakin ingin Log Out?")
				if (tes === true) {
					alert("Selamat Anda berhasil Log Out!")
					window.location="login.php"
				}
				else{
					window.location="index.php"
				}
			}
	</script>
	<div class="kotak_judul">
		<h1 class="judul"><a href="index.php" title="ilyamfaisal">ILY.am</a></h1>
		<div class="sosmed">
			<a href="https://www.facebook.com/ilyam.faisal.7" rel="nofollow" target="_blank" title="Facebook">
				<i class="fab fa-facebook-square"></i></i></a>
			<a href="https://www.instagram.com/ilyamfaisal/" rel="nofollow" target="_blank" title="Instagram">
				<i class="fab fa-instagram"></i></a>
			<a href="https://www.youtube.com/watch?v=3qj4yfZmDjc&t=22s" target="_blank" title="YouTube">
				<i class="fab fa-youtube-square"></i></a>
		</div>
	
	</div>
	<nav>
		<ul>
			<li><a href="index.php">Beranda</a></li>
			<li><a href="pemesanan.php">Pemesanan</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="program.php">Program</a></li>
			<li onclick="klik_logout();"><a href="#">Log Out</a></li>
			<li class="counter_">Anda pengunjung yang ke:</li>

		</ul>
	</nav>
	
	<div class="badan">
		<div class="content">
			<h1 class="jdartikel">Selamat Datang!</h1>
			<p class="tulisan">Perkenalkan nama saya Ilyam Faisal, saya saya sekarang mahasiswa di Universitas Ahmad Dahlan dan mengambil jurusan Teknik Informatika. Saya berasal dari desa kecil, di sebuah kabupaten yang bernama Karanganyar terletak di provinsi Jawa Tengah. Jika ada yang ingin kontak saya bisa lewat media sosial yang dapat diklik di Header paling atas web ini <i>yaa...</i> :).<br><br>Alasan mengapa saya mengambil jurusan teknik informatika ini yaitu, yang pertama, peluang pekerjaan kedepannya nanti masih sangat luas, terutama di Indonesia yang mana masih dalam tahap perkembangan. Walaupun saat ini perekonomian Indonesia masih bertumpu pada sektor bahan mentah, dan bukannya di sektor industri. Tetapi semakin cepatnya laju perkembangan revolusi industri dan teknologi dunia saat ini, mau tidak mau "memaksa" Indonesia kedepannya untuk memperhatikan pada sektor industri teknologi juga. Bahkan saat ini sudah mulai terlihat adanya perkembangan industri 4.0 di Indonesia yaitu dengan munculnya start up - start up teknologi, yang bahkan sudah ada yang menjadi Unicorn (status yang diberikan kepada start up atau perusahaan rintisan yang nilai valuasinya berhasil mencapai 1 miliar dolar AS atau sekitar Rp 14,1 triliun). Yaitu antara lain GoJek, Tokopedia, Traveloka, Bukalapak, OVO, dan JD.id. Tentunya hal tersebut merupakan awal yang baik, dan menjamin kedepannya terhadap adanya industri teknologi yang akan memajukan Indonesia. <br><br>Kemudian alasan yang kedua, saya memang suka dengan hal-hal problem solving, terutama pada komputer. Saya sering mengotak-atik laptop dan juga suka dengan mengedit foto ataupun video. Jika berbicara tentang impian kedepannya, maka saya ingin menjadi seorang software engineer dan <i>kepingin</i> juga jadi game developer. <br><br>Konsentrasi yang rencananya saya ambil saat semester 6 nanti adalah Relata (Rekayasa Perangkat Lunak dan Data). Sedangkan untuk bahasa pemrograman yang ingin saya kuasai kedepannya setelah C++ (masih belajar saat ini), yaitu Java, C#, dan Phyton. Untuk asisten yang paling menyebalkan menurut saya (karena saya tidak tahu nama semua asisten <i>jadi ya</i> untuk umum saja) adalah mereka yang kurang terbuka terhadap saran adik tingkatnya dan cenderung meremehkan, sudah itu saja <i>sih... </i>Terima kasih :) </p>
		</div>
	</div>
	<footer>
		<div class="bawah">
			<p>Copyright © 2020 <a href="index.php"><b>ILY.am</b></a> | Author & Design by <a href="https://www.instagram.com/ilyamfaisal/" target="_blank"><b>Ilyam Faisal</b></a></p>
		</div>
	</footer>
	
	<?php
		//buka file counter mode baca
		$filecounter="counter.txt";
		$fl=fopen($filecounter, "r+" );

		// ambil nilai hit dan simpan dalam variabel $hit
		$hit=fread($fl, filesize($filecounter));

		//tampilkan

		echo("<p class='counter'>$hit </p>");

		//tutup file counter.txt
		fclose($fl);

		//buka file counter.txt mode tulis
		$fl=fopen($filecounter,"w+");

		//tambahkan nilai hit dengan 1
		$hit=$hit+1;
		//simpan
		fwrite($fl,$hit,strlen($hit));
		//tutup
		fclose($fl);
	?>

<!-- -----------------ANIMASI---counter-------------------------------------------------------------->
	<script src="jquery.counterup.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
	<script>
		jQuery(document).ready(function( $ )
		{
		$('.counter').counterUp({
	    delay: 10,
	    time: 1000
	    });
	});
	</script>
	


</body>
</html>



