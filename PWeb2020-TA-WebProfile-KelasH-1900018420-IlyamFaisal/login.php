<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="style1.css">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300">
	
</head>
<body>		
	<script type="text/javascript">
			function login(){
				if (form.username.value != "" && form.password.value != "") {
					return true
				}
				else{
					alert("Username dan Password harus diisi!")
					return false
				}
			}
		</script>

		<form id="form" action="index.php" onsubmit="login()">
			<h1>Login</h3>
			<fieldset>
				
			<label class="" for="username">Username:</label>
			<input id="username" type="text" name="username" required="">

			<label class="" for="password">Password:</label>
			<input id="password" type="password" name="password" required="">
			
			<input type="checkbox" value="checkbox" name="checkbox" class="checkbox">
			<label class="light" for="checkbox">Simpan kata sandi</label><br>
			
			</fieldset>
			
			<button class="login" type="submit" onclick="login()">Login</button> 
			<button class="reset" type="reset">Reset</button> 
		
			<label class="lupa" for="lupa">Lupa password ? <a href="">Reset disini</a></label>
			<label class="akun" for="akun">Belum punya akun ? <a href="daftar.php">Daftar disini</a></label>
		
		</form>

		

	</body>
</html>