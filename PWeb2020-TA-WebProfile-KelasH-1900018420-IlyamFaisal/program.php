<!DOCTYPE html>
<html>
<head>
	<title>Program</title>
	<link rel="stylesheet" type="text/css" href="stylek.css">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

</head>
<body>
	<script type="text/javascript">
		function klik_logout(){
			var tes = confirm("Apakah Anda yakin ingin Log Out?")
				if (tes === true) {
					alert("Selamat Anda berhasil Log Out!")
					window.location="login.php"
				}
				else{
					window.location="index.php"
				}
			}
	</script>
	<div class="kotak_judul">
		<h1 class="judul"><a href="index.php" title="ilyamfaisal">ILY.am</a></h1>
		<div class="sosmed">
			<a href="https://www.facebook.com/ilyam.faisal.7" rel="nofollow" target="_blank" title="Facebook">
				<i class="fab fa-facebook-square"></i></i></a>
			<a href="https://www.instagram.com/ilyamfaisal/" rel="nofollow" target="_blank" title="Instagram">
				<i class="fab fa-instagram"></i></a>
			<a href="https://www.youtube.com/watch?v=3qj4yfZmDjc&t=22s" target="_blank" title="YouTube">
				<i class="fab fa-youtube-square"></i></a>
		</div>
	
	</div>
	<nav>
		<ul>
			<li><a href="index.php">Beranda</a></li>
			<li><a href="pemesanan.php">Pemesanan</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="program.php">Program</a></li>
			<li onclick="klik_logout();"><a href="#">Log Out</a></li>

		</ul>
	</nav>
	<div class="badan">
		<div class="content">
			<h1 class="jdartikel">Program Bilangan Prima</h1>
			
			<form action="" method="post">
				<input type="text" name="angka" class="angka" placeholder="Isikan Angka">
				<input type="submit" class="submit" name="kirim" value="Cek">
			</form>

			<?php 
			if (isset($_POST["kirim"])) {
			 	$nomor = trim($_POST["angka"]);
			 	if (is_numeric ($nomor)==true) {
			 		$prima = true;
			 		for ($i=2; $i < $nomor; $i++) { 
			 			if ($nomor % $i == 0){
			 				$prima = false;
			 			}
			 		}
			 		if ($nomor <= 1 ) {
			 			$prima = false;
			 		}
			 		if ($prima) {
			 			echo "<p class='kaki'>$nomor adalah Bilangan Prima</p>";
			 		}else{
			 			echo "<p class='kaki'>$nomor BUKAN Bilangan Prima</p>";
			 		}

			 	}else{
			 		echo "<p class='kaki'> Anda harus memasukkan angka dengan benar!</p>";
			 	}
			 }
			?> 
			<hr>
			<br>
			<h1 class="jdartikel">Penerapan Array</h1>
			<?php 
			function getMax($array){
				$n = count($array);
				$max = $array[0];
				for ($i=1; $i < $n; $i++) { 
					if ($max < $array[$i]) {
						$max = $array[$i];
					}
				}
				return $max;
			}

			function getMin($array){
				$n = count($array);
				$min = $array[0];
				for ($i=1; $i < $n; $i++) { 
					if ($min > $array[$i]) {
						$min = $array[$i];
					}
				}
				return $min;
			}
			
			$array = array(21, 233, 34, 98, 52, 90, 23, 11, 22, 555, 512);
			echo "Diketahui Array = (21, 233, 34, 98, 52, 90, 23, 11, 22, 555, 512)";
			echo "<p>Elemen terbesar => ";
			echo (getMax($array));
			echo "<br>";
			echo "<p>Elemen terkecil => ";
			echo (getMin($array));
			?>
		</div>
	</div>
	<footer>
		<div class="bawah">
			<p>Copyright © 2020 <a href="index.php"><b>ILY.am</b></a> | Author & Design by <a href="https://www.instagram.com/ilyamfaisal/" target="_blank"><b>Ilyam Faisal</b></a></p>
		</div>
	</footer>
	

</body>
</html>