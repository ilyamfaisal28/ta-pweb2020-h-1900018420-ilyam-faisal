<!DOCTYPE html>
<html>
<head>
	<title>About</title>
	<link rel="stylesheet" type="text/css" href="style5.css">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>
<body>
	<script type="text/javascript">
		function klik_logout(){
			var tes = confirm("Apakah Anda yakin ingin Log Out?")
				if (tes === true) {
					alert("Selamat Anda berhasil Log Out!")
					window.location="login.php"
				}
				else{
					window.location="index.php"
				}
			}
	</script>
	<div class="kotak_judul">
		<h1 class="judul"><a href="index.php" title="ilyamfaisal">ILY.am</a></h1>
		<div class="sosmed">
			<a href="https://www.facebook.com/ilyam.faisal.7" rel="nofollow" target="_blank" title="Facebook">
				<i class="fab fa-facebook-square"></i></i></a>
			<a href="https://www.instagram.com/ilyamfaisal/" rel="nofollow" target="_blank" title="Instagram">
				<i class="fab fa-instagram"></i></a>
			<a href="https://www.youtube.com/watch?v=3qj4yfZmDjc&t=22s" target="_blank" title="YouTube">
				<i class="fab fa-youtube-square"></i></a>
		</div>
	
	</div>
	<nav>
		<ul>
			<li><a href="index.php">Beranda</a></li>
			<li><a href="pemesanan.php">Pemesanan</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="program.php">Program</a></li>
			<li onclick="klik_logout();"><a href="#">Log Out</a></li>
		</ul>
	</nav>
	<div class="badan">
		<div class="content">
			<h1 class="jdartikel">Tentang Saya</h1>
			<p class="tulisan">
				Sebagai seorang "anak IT" tentunya mempunyai keluh kesah, yaitu kita sebagai "anak IT" dituntut & harus belajar terus menerus dan berkelanjutan serta mengikuti perkembangan yang ada di dunia IT. Karena teknologi berkembang terus menerus, begitu juga dengan bahasa pemrograman. Apabila kita tidak mengikuti perkembangan bahasa pemrograman, maka kita akan ketinggalan dan sulit untuk mendapat pekerjaan. Karena perusahaan tentunya juga sudah mengikuti dan menerapkan perkembangan teknologi, dalam hal ini bahasa pemrograman mereka kadang sudah yang paling update sehingga mereka membutuhkan pekerja yang sesuai dengan kriteria tersebut. <br><br>Kemudian untuk hal dimana basic konsentrasi saya. Saya belum mengetahui sepenuhnya dimana basic saya, karena saya sebelumnya belum pernah belajar pemrograman, dan saat ini masih dalam tahap awal belajar pemrograman. Tentunya masih sangat dini bagi saya untuk menilai dimana basic yang saya kuasai. <br><br>Untuk kelebihan saya yang berkaitan dengan bahasa pemrograman, sepertinya saya belum yakin juga, karena seperti yang sudah saya sebutkan di atas saya sebelumnya belum pernah belajar pemrograman sama sekali. Tapi saat ini saya masih berusaha untuk dapat menguasai bahasa C++, C#, Java, dan Phyton. Agar saya bisa meraih impian saya menjadi seorang software engineer ataupun menjadi game developer.
			</p>
		</div>
	</div>
	<footer>
		<div class="bawah">
			<p>Copyright © 2020 <a href="index.php"><b>ILY.am</b></a> | Author & Design by <a href="https://www.instagram.com/ilyamfaisal/" target="_blank"><b>Ilyam Faisal</b></a></p>
		</div>
	</footer>

</body>
</html>