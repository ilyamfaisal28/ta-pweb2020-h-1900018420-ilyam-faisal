<!DOCTYPE html>
<html>
<head>
	<title>Pemesanan</title>
	<link rel="stylesheet" type="text/css" href="style4.css">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<script type="text/javascript" src="jspemesanan.js" async></script>

</head>
<body>
	<div class="kotak_judul">
		<h1 class="judul"><a href="index.php" title="ilyamfaisal">ILY.am</a></h1>
		<div class="sosmed">
			<a href="https://www.facebook.com/ilyam.faisal.7" rel="nofollow" target="_blank" title="Facebook">
				<i class="fab fa-facebook-square"></i></i></a>
			<a href="https://www.instagram.com/ilyamfaisal/" rel="nofollow" target="_blank" title="Instagram">
				<i class="fab fa-instagram"></i></a>
			<a href="https://www.youtube.com/watch?v=3qj4yfZmDjc&t=22s" target="_blank" title="YouTube">
				<i class="fab fa-youtube-square"></i></a>
		</div>
	
	</div>
	<nav>
		<ul>
			<li><a href="index.php">Beranda</a></li>
			<li><a href="pemesanan.php">Pemesanan</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="program.php">Program</a></li>
			<li onclick="klik_logout();"><a href="#">Log Out</a></li>
		</ul>
	</nav>

	<div class="badan">
		<div class="content">
			<h1 class="jdartikel" align="center">Produk CD Game PS4</h1>
			<section class="container content-section">
				<div class="shop-items">
					<div class="barang">
						<span class="jdperbarang">Cyberpunk-2077</span>
						<img class="gambar-barang" src="gambar/Cyberpunk-2077.jpg">
						<div class="detail">
	                        <span class="harga">Rp.850rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				
			
					<div class="barang">
						<span class="jdperbarang">Red Dead Redemption II</span>
						<img class="gambar-barang" src="gambar/rdr2.jpeg">
						<div class="detail">
	                        <span class="harga">Rp.599rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				
		
					<div class="barang">
						<span class="jdperbarang">Sekiro</span>
						<img class="gambar-barang" src="gambar/sekiro.jpg">
						<div class="detail">
	                        <span class="harga">Rp.820rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				
			
					<div class="barang">
						<span class="jdperbarang">Spiderman</span>
						<img class="gambar-barang" src="gambar/spiderman.jpg">
						<div class="detail">
	                        <span class="harga">Rp.499rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				
				
					<div class="barang">
						<span class="jdperbarang">God of War 4</span>
						<img class="gambar-barang" src="gambar/gow4.jpg">
						<div class="detail">
	                        <span class="harga">Rp.399rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				
				
					<div class="barang">
						<span class="jdperbarang">FIFA 2021 Standard Edition</span>
						<img class="gambar-barang" src="gambar/fifa21standaredition.png">
						<div class="detail">
	                        <span class="harga">Rp.799rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				
					<div class="barang">
						<span class="jdperbarang">FIFA 2020</span>
						<img class="gambar-barang" src="gambar/cover.jpg">
						<div class="detail">
	                        <span class="harga">Rp.499rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				
					<div class="barang">
						<span class="jdperbarang">PES 2020</span>
						<img class="gambar-barang" src="gambar/pes20.jpg">
						<div class="detail">
	                        <span class="harga">Rp.450rb</span>
	                        <button class="btn btn-utama tombolkrj" type="button" title="Keranjang ada di paling bawah web ini">Tambah ke Keranjang</button>
                    	</div>
					</div>
				</div>
			</section>

			<h1 class="jdartikel2" align="center">Keranjang</h1>
			<section class="container content-section">
				<div class="cart-baris">
					<span class="cart-item cart-header cart-kolom">Barang</span>
					<span class="cart-harga cart-header cart-kolom">Harga</span>
					<span class="cart-kuantitas cart-header cart-kolom">Kuantitas</span>
				</div>
				<div class="cart-all-item">
					<!-- <div class="cart-baris">
						<div class="cart-item cart-kolom">
							<img class="cart-gbr" src="gambar/wireless-mouse-m545.png" height="100">
							<span class="cart-judul-item">Mouse Logitech wireless M545</span>
						</div>
						<span class="cart-harga cart-kolom">Rp.300rb</span>
						<div class="class-kuantitas cart-kolom">
							<input class="cart-kuantitas-input" type="number" value="2">
                        	<button class="btn btn-hapus" type="button">Hapus</button>
						</div>
					</div>
					<div class="cart-baris">
						<div class="cart-item cart-kolom">
							<img class="cart-gbr" src="gambar/asus rog strix gl12.jpg" height="100">
							<span class="cart-judul-item">PC Gaming 1 Set ASUS ROG Strix GL12</span>
						</div>
						<span class="cart-harga cart-kolom">Rp.25000rb</span>
						<div class="class-kuantitas cart-kolom">
							<input class="cart-kuantitas-input" type="number" value="1">
                        	<button class="btn btn-hapus" type="button">Hapus</button>
						</div>
					</div>
 -->
				</div>
				<div class="cart-total">
					<strong class="cart-total-judul">Total</strong>
					<span class="cart-total-harga">Rp.0</span>
				</div>
				<button class="btn btn-utama btn-beli" type="button">BELI</button>
			</section>
		</div>
	</div>
	<footer>
		<div class="bawah">
			<p>Copyright © 2020 <a href="index.php"><b>ILY.am</b></a> | Author & Design by <a href="https://www.instagram.com/ilyamfaisal/" target="_blank"><b>Ilyam Faisal</b></a></p>
		</div>
	</footer>
	
</body>
</html>