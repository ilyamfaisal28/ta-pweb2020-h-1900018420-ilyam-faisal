const form = document.getElementsById('form');
const name = document.getElementsById('name');
const email = document.getElementsById('email');
const alamat = document.getElementsById('alamat');
const telepon = document.getElementsById('telepon');

form.addEventListener('submit', (e) => {
	e.preventDefault();

	checkInputs();
})

function checkInputs() {
	const nameValue = name.value.trim();
	const emailValue = email.value.trim();
	const alamatValue = alamat.value.trim();
	const teleponValue = telepon.value.trim();

	if (nameValue === '') {
		alert('Nama harus diisi');

	}else{
		setSuccesFor(name);
	}
}